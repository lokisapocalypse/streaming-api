<?php

namespace Fusani\Streaming\Infrastructure\Persistence;

require_once 'vendor/autoload.php';

use Fusani\Streaming\Infrastructure;

$container = new Infrastructure\Container();
$container['config'] = require 'config/local.php';
$serviceContainer = new Infrastructure\ServiceContainer($container);

$service = $serviceContainer['MovieService'];
$service->importMovies();
