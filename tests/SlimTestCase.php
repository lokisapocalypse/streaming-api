<?php
//
// Unit Test Bootstrap and Slim PHP Testing Framework
// ============================================================================
//
// SlimpPHP is a little hard to test - but with this harness we can load our
// routes into our own `$app` container for unit testing, and then `run()` and
// hand a reference to the `$app` to our tests so that they have access to the
// dependency injection container and such.
//
// * Author: [Craig Davis](craig@there4development.com)
// * Since: 10/2/2013
//
// ----------------------------------------------------------------------------

namespace Fusani\Streaming;

use PHPUnit_Framework_TestCase;
use Slim\Slim;

class SlimTestCase extends PHPUnit_Framework_TestCase
{
    // We support these methods for testing. These are available via
    // `this->get()` and `$this->post()`. This is accomplished with the
    // `__call()` magic method below.
    private $testingMethods = array('get', 'post', 'patch', 'put', 'delete', 'head');

    // Run for each unit test to setup our slim app environment
    public function setup()
    {
        // Prepare Pimple.
        $container = new Infrastructure\Container();
        $container['config'] = require '../config/local.php';

        // Initialize our own copy of the slim application
        $app = new Slim(array(
            'version'        => '0.0.0',
            'debug'          => false,
            'mode'           => 'development',
        ));
        $container['app'] = $app;

        // Name this instance
        $app->setName('default');

        // Override provided EntityManager
        $container['EntityManager'] = function ($c) {
            return $this->getMockBuilder('Doctrine\Common\Persistence\ObjectManager')
                ->getMock();
        };

        // Declare our routes.
        require '../src/routes.php';

        // Establish a local reference to the Slim app object
        $this->app = $app;
        $this->container = $container;
    }

    // Abstract way to make a request to SlimPHP, this allows us to mock the
    // slim environment
    private function request($method, $path, $formVars = array(), $optionalHeaders = array())
    {
        // Capture STDOUT
        ob_start();

        // Instantiate the mock environment.
        $environment = array(
            'REQUEST_METHOD' => strtoupper($method),
            'PATH_INFO'      => $path,
            'SERVER_NAME'    => 'local.dev',
        );

        // GET variables are fed differently to the mock environment.
        if ($method == 'get') {
            $environment['QUERY_STRING'] = http_build_query($formVars);
        } else {
            if (is_string($formVars)) {
                $environment['slim.input'] = $formVars;
            } else {
                $environment['slim.input'] = http_build_query($formVars);
            }
        }

        \Slim\Environment::mock(array_merge($environment, $optionalHeaders));

        // Establish some useful references to the slim app properties
        $this->request  = $this->app->request();
        $this->response = $this->app->response();

        // Execute our app
        $this->app->run();

        // Return the application output. Also available in `response->body()`
        return ob_get_clean();
    }

    // Implement our `get`, `post`, and other http operations
    public function __call($method, $arguments)
    {
        if (in_array($method, $this->testingMethods)) {
            list($path, $formVars, $headers) = array_pad($arguments, 3, array());

            return $this->request($method, $path, $formVars, $headers);
        }

        throw new \BadMethodCallException(strtoupper($method) . ' is not supported');
    }
}
