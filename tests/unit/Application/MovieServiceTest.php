<?php


namespace Fusani\Streaming\Application;

use Fusani\Movies\Domain\Model\Movie;
use Fusani\Streaming\Domain\Model\Movie as StreamingMovie;
use Fusani\Streaming\Infrastructure\Persistence\FileSystem;
use Fusani\Streaming\Infrastructure\Persistence\InMemory;
use Fusani\Streaming\SimpleTestCase;

/**
 * @covers Fusani\Streaming\Application\MovieService
 */
class MovieServiceTest extends SimpleTestCase
{
    protected $fileSystem;
    protected $guideboxRepository;
    protected $movieRepository;
    protected $service;

    public function setup()
    {
        $this->movieRepository = new InMemory\MovieRepository();
        $this->guideboxRepository = $this->getMockBuilder(Movie\MovieRepository::class)
            ->disableOriginalConstructor()
            ->getMock();
        $this->fileSystem = $this->getMockBuilder(FileSystem\MovieRepository::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->service = new MovieService(
            $this->movieRepository,
            $this->guideboxRepository,
            new StreamingMovie\MovieBuilder(),
            $this->fileSystem
        );
    }

    public function testImportMoviesWithNoMoviesDoesntAddMovies()
    {
        $this->assertEquals(0, $this->movieRepository->count());

        $this->guideboxRepository->expects($this->once())
            ->method('many')
            ->will($this->returnValue([]));

        $this->service->importMovies();

        $this->assertEquals(0, $this->movieRepository->count());
    }

    public function testImportMoviesWithMoviesAddsMoviesWithExternalId()
    {
        $this->assertEquals(0, $this->movieRepository->count());

        $movie = new Movie\Movie('guardians-of-the-galaxy', 'Guardians of the Galaxy', 'movie', 2012);
        $movieTwo = new Movie\Movie('ghostbusters', 'Ghostbusters', 'movie', 1984);

        $this->guideboxRepository->expects($this->once())
            ->method('many')
            ->will($this->returnValue([$movie, $movieTwo]));
        $this->guideboxRepository->expects($this->exactly(2))
            ->method('oneOfId')
            ->will($this->onConsecutiveCalls($movie, $movieTwo));

        $this->service->importMovies();

        $this->assertEquals(2, $this->movieRepository->count());
    }
}
