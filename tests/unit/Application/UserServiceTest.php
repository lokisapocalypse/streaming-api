<?php


namespace Fusani\Streaming\Application;

use Fusani\Streaming\Infrastructure\Persistence\InMemory;
use Fusani\Streaming\SimpleTestCase;

/**
 * @covers Fusani\Streaming\Application\UserService
 */
class UserServiceTest extends SimpleTestCase
{
    protected $repository;
    protected $service;

    protected function expected()
    {
        return  [
            'identity' => 'spider-man',
            'firstname' => 'Peter',
            'lastname' => 'Parker',
            'email' => 'peter.parker@dailybugle.com',
        ];
    }

    public function setup()
    {
        $this->repository = new InMemory\UserRepository();
        $this->service = new UserService($this->repository);
    }

    public function testCreateUserThatDoesNotExist()
    {
        $this->assertEquals($this->expected(), $this->service->createUser('spider-man', 'Peter', 'Parker', 'peter.parker@dailybugle.com'));
        $this->assertEquals(1, $this->repository->count());
        $user = $this->repository->oneOfIdentity('spider-man');
        $this->assertNotNull($user);
        $this->assertInstanceOf('Fusani\Streaming\Domain\Model\User\User', $user);
    }

    public function testCreateUserThatAlreadyExists()
    {
        $this->assertEquals($this->expected(), $this->service->createUser('spider-man', 'Peter', 'Parker', 'peter.parker@dailybugle.com'));
        $this->assertEquals($this->expected(), $this->service->createUser('spider-man', 'Peter', 'Parker', 'peter.parker@dailybugle.com'));
        $this->assertEquals(1, $this->repository->count());
    }
}
