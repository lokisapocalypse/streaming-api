<?php

namespace Fusani\Streaming\Application;

use Fusani\Streaming\SimpleTestCase;
use Fusani\Streaming\Infrastructure\Persistence\InMemory;

/**
 * @covers Fusani\Streaming\Application\UserServiceFactory
 */
class UserServiceFactoryTest extends SimpleTestCase
{
    public function testCreateService()
    {
        $factory = new UserServiceFactory();
        $service = $factory->createService($this->container);
        $this->assertNotNull($service);
        $this->assertInstanceOf('Fusani\Streaming\Application\UserService', $service);
    }
}
