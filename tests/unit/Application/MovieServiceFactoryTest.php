<?php

namespace Fusani\Streaming\Application;

use Fusani\Streaming\SimpleTestCase;
use Fusani\Streaming\Infrastructure\Persistence\InMemory;

/**
 * @covers Fusani\Streaming\Application\MovieServiceFactory
 */
class MovieServiceFactoryTest extends SimpleTestCase
{
    public function testCreateService()
    {
        $factory = new MovieServiceFactory();
        $service = $factory->createService($this->container);
        $this->assertNotNull($service);
        $this->assertInstanceOf('Fusani\Streaming\Application\MovieService', $service);
    }
}
