<?php

namespace Fusani\Streaming\Domain\Model\Movie;

use Fusani\Streaming\SimpleTestCase;

/**
 * @covers Fusani\Streaming\Domain\Model\Movie\ExternalId
 */
class ExternalIdTest extends SimpleTestCase
{
    protected $externalId;

    public function setup()
    {
        $this->externalId = new ExternalId('streaming', 'Fusani site');
    }

    public function testConstructor()
    {
        $this->assertEquals('streaming', \PHPUnit_Framework_Assert::readAttribute($this->externalId, 'externalId'));
        $this->assertEquals('Fusani site', \PHPUnit_Framework_Assert::readAttribute($this->externalId, 'source'));
    }

    public function testIdentity()
    {
        $this->assertEquals('streamingFusani site', $this->externalId->identity());
    }

    public function testOfMovie()
    {
        $movie = new Movie('Guardians of the Galaxy', 'movie', 2012);

        $externalId = $this->externalId->ofMovie($movie);

        $this->assertEquals($movie, \PHPUnit_Framework_Assert::readAttribute($this->externalId, 'movie'));
        $this->assertNotNull($externalId);
        $this->assertInstanceOf(ExternalId::class, $externalId);
    }
}
