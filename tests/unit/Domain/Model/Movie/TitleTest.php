<?php

namespace Fusani\Streaming\Domain\Model\Movie;

use Fusani\Streaming\SimpleTestCase;

/**
 * @covers Fusani\Streaming\Domain\Model\Movie\Title
 */
class TitleTest extends SimpleTestCase
{
    protected $title;

    public function setup()
    {
        $this->title = new Title('Guardians of the Galaxy (US)');
    }

    public function testConstructor()
    {
        $this->assertEquals('Guardians of the Galaxy (US)', \PHPUnit_Framework_Assert::readAttribute($this->title, 'title'));
    }

    public function testIdentity()
    {
        $this->assertEquals('Guardians of the Galaxy (US)', $this->title->identity());
    }

    public function testOfMovie()
    {
        $movie = new Movie('Guardians of the Galaxy', 'movie', 2012);

        $title = $this->title->ofMovie($movie);

        $this->assertNotNull($title);
        $this->assertInstanceOf(Title::class, $title);
    }
}
