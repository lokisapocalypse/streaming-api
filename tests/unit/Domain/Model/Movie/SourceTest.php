<?php

namespace Fusani\Streaming\Domain\Model\Movie;

use Fusani\Streaming\SimpleTestCase;

/**
 * @covers Fusani\Streaming\Domain\Model\Movie\Source
 */
class SourceTest extends SimpleTestCase
{
    protected $source;

    public function setup()
    {
        $this->source = new Source('subscription', 'Netflix', 'www.netflix.com');
    }

    public function testIdentity()
    {
        $this->assertEquals('www.netflix.comNetflixsubscription', $this->source->identity());
    }

    public function testOfMovie()
    {
        $movie = new Movie('Ghostbusters', 'movie', 1989);
        $source = $this->source->ofMovie($movie);

        $this->assertNotNull($source);
        $this->assertInstanceOf(Source::class, $source);
    }
}
