<?php

namespace Fusani\Streaming\Domain\Model\Movie;

use Fusani\Movies\Domain\Model\Movie as FusaniMovie;
use Fusani\Streaming\SimpleTestCase;

/**
 * @covers Fusani\Streaming\Domain\Model\Movie\MovieBuilder
 */
class MovieBuilderTest extends SimpleTestCase
{
    protected $builder;
    protected $movie;

    public function setup()
    {
        $this->builder = new MovieBuilder();
        $this->movie = new FusaniMovie\Movie('guardians-of-the-galaxy', 'Guardians of the Galaxy', 'movie', 2012);
    }

    public function testBuildFromMovieObjectNoAlternateTitles()
    {
        $streamingMovie = $this->builder->buildFromMovieObject($this->movie);

        $this->assertNotNull($streamingMovie);
        $this->assertInstanceOf(Movie::class, $streamingMovie);

        $interest = $streamingMovie->provideSitemapInterest();
        $expected = [
            'title' => 'Guardians of the Galaxy',
            'type' => 'movie',
            'year' => 2012,
        ];

        $this->assertEquals($expected, $interest);

        $alternateTitles = \PHPUnit_Framework_Assert::readAttribute($streamingMovie, 'alternateTitles');
        $externalIds = \PHPUnit_Framework_Assert::readAttribute($streamingMovie, 'externalIds');
        $posters = \PHPUnit_Framework_Assert::readAttribute($streamingMovie, 'posters');

        $this->assertEquals(0, count($alternateTitles));
        $this->assertEquals(1, count($externalIds));
        $this->assertEquals(0, count($posters));
    }

    public function testBuildFromMovieObjectWithAlternateTitles()
    {
        $this->movie->addAlternateTitle('Guardians of the Galaxy (US)');

        $streamingMovie = $this->builder->buildFromMovieObject($this->movie);

        $alternateTitles = \PHPUnit_Framework_Assert::readAttribute($streamingMovie, 'alternateTitles');
        $this->assertEquals(1, count($alternateTitles));
    }

    public function testBuildFromMovieObjectWithCast()
    {
        $this->movie->addCast('Chris Pratt', 'Peter Quill');

        $streamingMovie = $this->builder->buildFromMovieObject($this->movie);

        $cast = \PHPUnit_Framework_Assert::readAttribute($streamingMovie, 'cast');
        $this->assertEquals(1, count($cast));
    }

    public function testBuildFromMovieObjectWithDirectors()
    {
        $this->movie->addDirector('James Gunn');

        $streamingMovie = $this->builder->buildFromMovieObject($this->movie);

        $directors = \PHPUnit_Framework_Assert::readAttribute($streamingMovie, 'directors');
        $this->assertEquals(1, count($directors));
    }

    public function testBuildFromMovieObjectWithKeywords()
    {
        $this->movie->addKeyword('comic');

        $streamingMovie = $this->builder->buildFromMovieObject($this->movie);

        $keywords = \PHPUnit_Framework_Assert::readAttribute($streamingMovie, 'keywords');
        $this->assertEquals(1, count($keywords));
    }

    public function testBuildFromMovieObjectWithPosters()
    {
        $this->movie->addPoster('www.image.com', 'poster');

        $streamingMovie = $this->builder->buildFromMovieObject($this->movie);

        $posters = \PHPUnit_Framework_Assert::readAttribute($streamingMovie, 'posters');
        $this->assertEquals(1, count($posters));
    }

    public function testBuildFromMovieObjectWithReviews()
    {
        $this->movie->addReview('It was good', 'George Clooney', 'www.truth.com');

        $streamingMovie = $this->builder->buildFromMovieObject($this->movie);

        $reviews = \PHPUnit_Framework_Assert::readAttribute($streamingMovie, 'reviews');
        $this->assertEquals(1, count($reviews));
    }

    public function testBuildFromMovieObjectWithSources()
    {
        $this->movie->addSource('subscription', 'Netflix', 'www.netflix.com');

        $streamingMovie = $this->builder->buildFromMovieObject($this->movie);

        $sources = \PHPUnit_Framework_Assert::readAttribute($streamingMovie, 'sources');
        $this->assertEquals(1, count($sources));
    }
}
