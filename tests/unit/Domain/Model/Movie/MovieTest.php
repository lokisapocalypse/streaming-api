<?php

namespace Fusani\Streaming\Domain\Model\Movie;

use Fusani\Streaming\SimpleTestCase;

/**
 * @covers Fusani\Streaming\Domain\Model\Movie\Movie
 */
class MovieTest extends SimpleTestCase
{
    protected $movie;

    public function setup()
    {
        $this->movie = new Movie('Guardians of the Galaxy', 'movie', 2012);
    }

    public function testConstructor()
    {
        $this->assertEquals([], \PHPUnit_Framework_Assert::readAttribute($this->movie, 'alternateTitles'));
        $this->assertEquals([], \PHPUnit_Framework_Assert::readAttribute($this->movie, 'cast'));
        $this->assertEquals([], \PHPUnit_Framework_Assert::readAttribute($this->movie, 'channels'));
        $this->assertEquals([], \PHPUnit_Framework_Assert::readAttribute($this->movie, 'directors'));
        $this->assertEquals([], \PHPUnit_Framework_Assert::readAttribute($this->movie, 'episodes'));
        $this->assertEquals([], \PHPUnit_Framework_Assert::readAttribute($this->movie, 'externalIds'));
        $this->assertEquals([], \PHPUnit_Framework_Assert::readAttribute($this->movie, 'genre'));
        $this->assertEquals([], \PHPUnit_Framework_Assert::readAttribute($this->movie, 'posters'));
        $this->assertEquals([], \PHPUnit_Framework_Assert::readAttribute($this->movie, 'reviews'));
        $this->assertEquals([], \PHPUnit_Framework_Assert::readAttribute($this->movie, 'socialMedia'));
        $this->assertEquals([], \PHPUnit_Framework_Assert::readAttribute($this->movie, 'sources'));
        $this->assertEquals([], \PHPUnit_Framework_Assert::readAttribute($this->movie, 'statistics'));
        $this->assertEquals([], \PHPUnit_Framework_Assert::readAttribute($this->movie, 'tags'));
        $this->assertEquals([], \PHPUnit_Framework_Assert::readAttribute($this->movie, 'trailers'));
        $this->assertEquals([], \PHPUnit_Framework_Assert::readAttribute($this->movie, 'writers'));

        $this->assertEquals(2012, \PHPUnit_Framework_Assert::readAttribute($this->movie, 'releaseYear'));
        $this->assertEquals('Guardians of the Galaxy', \PHPUnit_Framework_Assert::readAttribute($this->movie, 'title'));
        $this->assertEquals('movie', \PHPUnit_Framework_Assert::readAttribute($this->movie, 'type'));
    }

    public function testAddAlternateTitleWithNoneSet()
    {
        $movie = $this->movie->addAlternateTitle('Guardians of the Galaxy (US)');

        $this->assertNotNull($movie);
        $this->assertInstanceOf(Movie::class, $movie);

        $this->assertNotEquals([], \PHPUnit_Framework_Assert::readAttribute($movie, 'alternateTitles'));
        $this->assertEquals(1, count(\PHPUnit_Framework_Assert::readAttribute($movie, 'alternateTitles')));
    }

    public function testAddAlternateTitleDoesntAddDuplicates()
    {
        $movie = $this->movie->addAlternateTitle('Guardians of the Galaxy (US)');
        $movie = $this->movie->addAlternateTitle('Guardians of the Galaxy (US)');

        $this->assertNotNull($movie);
        $this->assertInstanceOf(Movie::class, $movie);

        $this->assertNotEquals([], \PHPUnit_Framework_Assert::readAttribute($movie, 'alternateTitles'));
        $this->assertEquals(1, count(\PHPUnit_Framework_Assert::readAttribute($movie, 'alternateTitles')));
    }

    public function testAddCastWithNoneSet()
    {
        $movie = $this->movie->addCast(['actor' => 'Bill Murray', 'character' => 'Peter Venkman']);

        $this->assertNotNull($movie);
        $this->assertInstanceOf(Movie::class, $movie);

        $this->assertNotEquals([], \PHPUnit_Framework_Assert::readAttribute($movie, 'cast'));
        $this->assertEquals(1, count(\PHPUnit_Framework_Assert::readAttribute($movie, 'cast')));
    }

    public function testAddCastDoesntAddDuplicates()
    {
        $movie = $this->movie->addCast(['actor' => 'Bill Murray', 'character' => 'Peter Venkman']);
        $movie = $this->movie->addCast(['actor' => 'Bill Murray', 'character' => 'Peter Venkman']);

        $this->assertNotNull($movie);
        $this->assertInstanceOf(Movie::class, $movie);

        $this->assertNotEquals([], \PHPUnit_Framework_Assert::readAttribute($movie, 'cast'));
        $this->assertEquals(1, count(\PHPUnit_Framework_Assert::readAttribute($movie, 'cast')));
    }

    public function testAddDirectorWithNoneSet()
    {
        $movie = $this->movie->addDirector('Ivan Reitman');

        $this->assertNotNull($movie);
        $this->assertInstanceOf(Movie::class, $movie);

        $this->assertNotEquals([], \PHPUnit_Framework_Assert::readAttribute($movie, 'directors'));
        $this->assertEquals(1, count(\PHPUnit_Framework_Assert::readAttribute($movie, 'directors')));
    }

    public function testAddDirectorDoesntAddDuplicates()
    {
        $movie = $this->movie->addDirector('Ivan Reitman');
        $movie = $this->movie->addDirector('Ivan Reitman');

        $this->assertNotNull($movie);
        $this->assertInstanceOf(Movie::class, $movie);

        $this->assertNotEquals([], \PHPUnit_Framework_Assert::readAttribute($movie, 'directors'));
        $this->assertEquals(1, count(\PHPUnit_Framework_Assert::readAttribute($movie, 'directors')));
    }

    public function testAddExternalId()
    {
        $movie = $this->movie->addExternalId('streaming', 'Fusani');

        $this->assertNotNull($movie);
        $this->assertInstanceOf(Movie::class, $movie);

        $this->assertNotEquals([], \PHPUnit_Framework_Assert::readAttribute($movie, 'externalIds'));
        $this->assertEquals(1, count(\PHPUnit_Framework_Assert::readAttribute($movie, 'externalIds')));
    }

    public function testAddExternalIdDoesntAddDuplicates()
    {
        $movie = $this->movie->addExternalId('streaming', 'Fusani');
        $movie = $this->movie->addExternalId('streaming', 'Fusani');

        $this->assertNotNull($movie);
        $this->assertInstanceOf(Movie::class, $movie);

        $this->assertNotEquals([], \PHPUnit_Framework_Assert::readAttribute($movie, 'externalIds'));
        $this->assertEquals(1, count(\PHPUnit_Framework_Assert::readAttribute($movie, 'externalIds')));
    }

    public function testAddKeyword()
    {
        $movie = $this->movie->addKeyword('comedy');

        $this->assertNotNull($movie);
        $this->assertInstanceOf(Movie::class, $movie);

        $this->assertNotEquals([], \PHPUnit_Framework_Assert::readAttribute($movie, 'keywords'));
        $this->assertEquals(1, count(\PHPUnit_Framework_Assert::readAttribute($movie, 'keywords')));
    }

    public function testAddKeywordDoesntAddDuplicates()
    {
        $movie = $this->movie->addKeyword('comedy');
        $movie = $this->movie->addKeyword('comedy');

        $this->assertNotNull($movie);
        $this->assertInstanceOf(Movie::class, $movie);

        $this->assertNotEquals([], \PHPUnit_Framework_Assert::readAttribute($movie, 'keywords'));
        $this->assertEquals(1, count(\PHPUnit_Framework_Assert::readAttribute($movie, 'keywords')));
    }

    public function testAddPoster()
    {
        $movie = $this->movie->addPoster('www.posters.com', 'small');

        $this->assertNotNull($movie);
        $this->assertInstanceOf(Movie::class, $movie);

        $this->assertNotEquals([], \PHPUnit_Framework_Assert::readAttribute($movie, 'posters'));
        $this->assertEquals(1, count(\PHPUnit_Framework_Assert::readAttribute($movie, 'posters')));
    }

    public function testAddPosterDoesntAddDuplicates()
    {
        $movie = $this->movie->addPoster('www.posters.com', 'small');
        $movie = $this->movie->addPoster('www.posters.com', 'small');

        $this->assertNotNull($movie);
        $this->assertInstanceOf(Movie::class, $movie);

        $this->assertNotEquals([], \PHPUnit_Framework_Assert::readAttribute($movie, 'posters'));
        $this->assertEquals(1, count(\PHPUnit_Framework_Assert::readAttribute($movie, 'posters')));
    }

    public function testAddReview()
    {
        $review = [
            'author' => 'George Clooney',
            'review' => 'It was funny',
            'title' => 'Best. Movie. Ever.',
            'link' => 'www.truth.com',
        ];
        $movie = $this->movie->addReview($review);

        $this->assertNotNull($movie);
        $this->assertInstanceOf(Movie::class, $movie);

        $this->assertNotEquals([], \PHPUnit_Framework_Assert::readAttribute($movie, 'reviews'));
        $this->assertEquals(1, count(\PHPUnit_Framework_Assert::readAttribute($movie, 'reviews')));
    }

    public function testAddReviewDoesntAddDuplicates()
    {
        $review = [
            'author' => 'George Clooney',
            'review' => 'It was funny',
            'title' => 'Best. Movie. Ever.',
            'link' => 'www.truth.com',
        ];
        $movie = $this->movie->addReview($review);
        $movie = $this->movie->addReview($review);

        $this->assertNotNull($movie);
        $this->assertInstanceOf(Movie::class, $movie);

        $this->assertNotEquals([], \PHPUnit_Framework_Assert::readAttribute($movie, 'reviews'));
        $this->assertEquals(1, count(\PHPUnit_Framework_Assert::readAttribute($movie, 'reviews')));
    }

    public function testAddSource()
    {
        $source = [
            'details' => [],
            'link' => 'www.netflix.com',
            'name' => 'Netflix',
            'type' => 'free',
        ];
        $movie = $this->movie->addSource($source);

        $this->assertNotNull($movie);
        $this->assertInstanceOf(Movie::class, $movie);

        $this->assertNotEquals([], \PHPUnit_Framework_Assert::readAttribute($movie, 'sources'));
        $this->assertEquals(1, count(\PHPUnit_Framework_Assert::readAttribute($movie, 'sources')));
    }

    public function testAddSourceDoesntAddDuplicates()
    {
        $source = [
            'details' => [],
            'link' => 'www.netflix.com',
            'name' => 'Netflix',
            'type' => 'free',
        ];
        $movie = $this->movie->addSource($source);
        $movie = $this->movie->addSource($source);

        $this->assertNotNull($movie);
        $this->assertInstanceOf(Movie::class, $movie);

        $this->assertNotEquals([], \PHPUnit_Framework_Assert::readAttribute($movie, 'sources'));
        $this->assertEquals(1, count(\PHPUnit_Framework_Assert::readAttribute($movie, 'sources')));
    }

    public function testIdentity()
    {
        $this->assertEmpty($this->movie->identity());
    }

    public function testProvideSitemapInterest()
    {
        $expected = [
            'title' => 'Guardians of the Galaxy',
            'type' => 'movie',
            'year' => 2012,
        ];

        $this->assertEquals($expected, $this->movie->provideSitemapInterest());
    }

    public function testSetPlot()
    {
        $movie = $this->movie->setPlot('Ghosts invade NY');
        $this->assertNotNull($movie);
        $this->assertInstanceOf(Movie::class, $movie);
        $this->assertEquals('Ghosts invade NY',  \PHPUnit_Framework_Assert::readAttribute($movie, 'plot'));
    }

    public function testSetRating()
    {
        $movie = $this->movie->setRating('PG-13');

        $this->assertNotNull($movie);
        $this->assertInstanceOf(Movie::class, $movie);

        $this->assertEquals('PG-13', \PHPUnit_Framework_Assert::readAttribute($movie, 'rating'));
    }
}
