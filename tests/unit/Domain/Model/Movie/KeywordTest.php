<?php

namespace Fusani\Streaming\Domain\Model\Movie;

use Fusani\Streaming\SimpleTestCase;

/**
 * @covers Fusani\Streaming\Domain\Model\Movie\Keyword
 */
class KeywordTest extends SimpleTestCase
{
    protected $keyword;

    public function setup()
    {
        $this->keyword = new Keyword('comedy');
    }

    public function testIdentity()
    {
        $this->assertEquals('comedy', $this->keyword->identity());
    }

    public function testOfMovie()
    {
        $movie = new Movie('Ghostbusters', 'movie', 1989);
        $keyword = $this->keyword->ofMovie($movie);

        $this->assertNotNull($keyword);
        $this->assertInstanceOf(Keyword::class, $keyword);
    }
}
