<?php

namespace Fusani\Streaming\Domain\Model\Movie;

use Fusani\Streaming\SimpleTestCase;

/**
 * @covers Fusani\Streaming\Domain\Model\Movie\Cast
 */
class CastTest extends SimpleTestCase
{
    protected $cast;

    public function setup()
    {
        $this->cast = new Cast('Bill Murray', 'Peter Venkman');
    }

    public function testIdentity()
    {
        $this->assertEquals('Bill MurrayPeter Venkman', $this->cast->identity());
    }

    public function testOfMovie()
    {
        $movie = new Movie('Ghostbusters', 'movie', 1989);
        $cast = $this->cast->ofMovie($movie);

        $this->assertNotNull($cast);
        $this->assertInstanceOf(Cast::class, $cast);
    }
}
