<?php

namespace Fusani\Streaming\Domain\Model\Movie;

use Fusani\Streaming\SimpleTestCase;

/**
 * @covers Fusani\Streaming\Domain\Model\Movie\Director
 */
class DirectorTest extends SimpleTestCase
{
    protected $director;

    public function setup()
    {
        $this->director = new Director('Ivan Reitman');
    }

    public function testIdentity()
    {
        $this->assertEquals('Ivan Reitman', $this->director->identity());
    }

    public function testOfMovie()
    {
        $movie = new Movie('Ghostbusters', 'movie', 1989);
        $director = $this->director->ofMovie($movie);

        $this->assertNotNull($director);
        $this->assertInstanceOf(Director::class, $director);
    }
}
