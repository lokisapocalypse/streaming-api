<?php

namespace Fusani\Streaming\Domain\Model\Movie;

use Fusani\Streaming\SimpleTestCase;

/**
 * @covers Fusani\Streaming\Domain\Model\Movie\Channel
 */
class ChannelTest extends SimpleTestCase
{
    public function testConstructor()
    {
        $channel = new Channel();

        $this->assertEquals([], \PHPUnit_Framework_Assert::readAttribute($channel, 'externalIds'));
        $this->assertEquals([], \PHPUnit_Framework_Assert::readAttribute($channel, 'liveStreams'));
        $this->assertEquals([], \PHPUnit_Framework_Assert::readAttribute($channel, 'posters'));
        $this->assertEquals([], \PHPUnit_Framework_Assert::readAttribute($channel, 'socialMedia'));
    }
}
