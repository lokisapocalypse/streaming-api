<?php

namespace Fusani\Streaming\Domain\Model\Movie;

use Fusani\Streaming\SimpleTestCase;

/**
 * @covers Fusani\Streaming\Domain\Model\Movie\Review
 */
class ReviewTest extends SimpleTestCase
{
    protected $review;

    public function setup()
    {
        $this->review = new Review('George Clooney', 'It was good', 'Best. Movie. Ever.', 'www.truth.com');
    }

    public function testIdentity()
    {
        $this->assertEquals('George Clooneywww.truth.comIt was goodBest. Movie. Ever.', $this->review->identity());
    }

    public function testOfMovie()
    {
        $movie = new Movie('Ghostbusters', 'movie', 1989);
        $review = $this->review->ofMovie($movie);

        $this->assertNotNull($review);
        $this->assertInstanceOf(Review::class, $review);
    }
}
