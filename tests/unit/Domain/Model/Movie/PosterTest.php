<?php

namespace Fusani\Streaming\Domain\Model\Movie;

use Fusani\Streaming\SimpleTestCase;

/**
 * @covers Fusani\Streaming\Domain\Model\Movie\Poster
 */
class PosterTest extends SimpleTestCase
{
    protected $poster;

    public function setup()
    {
        $this->poster = new Poster('www.posters.com', 'small');
    }

    public function testConstructor()
    {
        $this->assertEquals('www.posters.com', \PHPUnit_Framework_Assert::readAttribute($this->poster, 'link'));
        $this->assertEquals('small', \PHPUnit_Framework_Assert::readAttribute($this->poster, 'size'));
    }

    public function testIdentity()
    {
        $this->assertEquals('www.posters.comsmall', $this->poster->identity());
    }

    public function testOfMovie()
    {
        $poster = $this->poster->ofMovie(new Movie('Guardians of the Galaxy', 'movie', 2012));

        $this->assertNotNull($poster);
        $this->assertInstanceOf(Poster::class, $poster);
    }
}
