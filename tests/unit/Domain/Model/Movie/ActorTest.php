<?php

namespace Fusani\Streaming\Domain\Model\Movie;

use Fusani\Streaming\SimpleTestCase;

/**
 * @covers Fusani\Streaming\Domain\Model\Movie\Actor
 */
class ActorTest extends SimpleTestCase
{
    public function testConstructor()
    {
        $actor = new Actor('Chris Pratt');

        $this->assertEquals('Chris Pratt', \PHPUnit_Framework_Assert::readAttribute($actor, 'name'));
    }
}
