<?php

namespace Fusani\Streaming\Domain\Model\User;

use Fusani\Streaming\SimpleTestCase;

/**
 * @covers Fusani\Streaming\Domain\Model\User\User
 */
class UserTest extends SimpleTestCase
{
    public function testGetUserInterest()
    {
        $expected = [
            'identity' => 'spider-man',
            'firstname' => 'Peter',
            'lastname' => 'Parker',
            'email' => 'peter.parker@dailybugle.com',
        ];
        $user = new User('spider-man', 'Peter', 'Parker', 'peter.parker@dailybugle.com');
        $this->assertEquals($expected, $user->getUserInterest());
    }
}
