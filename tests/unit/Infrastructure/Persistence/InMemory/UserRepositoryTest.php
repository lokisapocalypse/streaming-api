<?php

namespace Fusani\Streaming\Infrastructure\Persistence\InMemory;

use Fusani\Streaming\SimpleTestCase;
use Fusani\Streaming\Domain\Model\User;

/**
 * @covers Fusani\Streaming\Infrastructure\Persistence\InMemory\UserRepository
 */
class UserRepositoryTest extends SimpleTestCase
{
    protected $repository;

    public function setup()
    {
        $this->repository = new UserRepository();
    }

    public function testCount()
    {
        $user = new User\User('Peter', 'Parker', 'peter.parker@dailybugle.com', 'Daily Bugle');
        $this->repository->add($user);

        $this->assertEquals(1, $this->repository->count());
    }

    public function testOneOfIdentity()
    {
        $user = new User\User('Peter', 'Parker', 'peter.parker@dailybugle.com', 'Daily Bugle');
        $interest = $user->getUserInterest();

        $this->assertNull($this->repository->oneOfIdentity($interest['identity']));

        $this->repository->add($user);
        $this->repository->flush();

        $this->assertEquals($user, $this->repository->oneOfIdentity($interest['identity']));
    }
}
