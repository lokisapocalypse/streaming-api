<?php

namespace Fusani\Streaming\Infrastructure\Persistence\InMemory;

use Fusani\Streaming\SimpleTestCase;
use Fusani\Streaming\Domain\Model\Movie;

/**
 * @covers Fusani\Streaming\Infrastructure\Persistence\InMemory\MovieRepository
 */
class MovieRepositoryTest extends SimpleTestCase
{
    protected $repository;

    public function setup()
    {
        $this->repository = new MovieRepository();
    }

    public function testConstructor()
    {
        $this->assertEquals([], \PHPUnit_Framework_Assert::readAttribute($this->repository, 'movies'));
        $this->assertEquals(0, $this->repository->count());
    }

    public function testCount()
    {
        $movie = new Movie\Movie('Guardians of the Galaxy', 'movie', 2012);
        $this->repository->add($movie);

        $this->assertEquals(1, $this->repository->count());
    }

    public function testLatestGuideboxIndex()
    {
        $this->assertEquals(-1, $this->repository->latestGuideboxIndex());
        $this->repository->add(new Movie\Movie('Guardians of the Galaxy', 'movie', 2012));
        $this->assertEquals(0, $this->repository->latestGuideboxIndex());
        $this->repository->add(new Movie\Movie('Guardians of the Galaxy', 'movie', 2012));
        $this->assertEquals(1, $this->repository->latestGuideboxIndex());
    }
}
