<?php

namespace Fusani\Streaming\Infrastructure\JsonRpc\Transport;

use Fusani\Streaming\SimpleTestCase;

/**
 * @covers Fusani\Streaming\Infrastructure\JsonRpc\Transport\BasicServer
 */
class BasicServerTest extends SimpleTestCase
{
    public function testReply()
    {
        $server = new BasicServer();

        ob_start();
        $server->reply('test data');
        $output = ob_get_clean();

        $this->assertEquals('test data', $output);
    }
}
