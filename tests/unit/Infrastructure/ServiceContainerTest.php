<?php

namespace Fusani\Streaming\Infrastructure;

use Fusani\Streaming\SimpleTestCase;

/**
 * @covers Fusani\Streaming\Infrastructure\ServiceContainer
 */
class ServiceContainerTest extends SimpleTestCase
{
    public function testConstructor()
    {
        $container = new Container();
        $serviceContainer = new ServiceContainer($container);
        $this->assertTrue($serviceContainer instanceof ServiceContainer);
    }
}
