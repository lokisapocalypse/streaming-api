<?php

namespace Fusani\Streaming\Infrastructure;

use Fusani\Streaming\SimpleTestCase;

/**
 * @covers Fusani\Streaming\Infrastructure\Container
 */
class ContainerTest extends SimpleTestCase
{
    public function testConstructor()
    {
        $container = new Container();
        $this->assertTrue($container instanceof Container);
    }
}
