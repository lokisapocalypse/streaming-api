<?php

namespace Fusani\Streaming;

use JsonRpc;
use Slim;

class SlimTransport extends JsonRpc\Transport\BasicServer
{
    protected $app;

    public function __construct(Slim\Slim $app)
    {
        $this->app = $app;
    }

    public function receive()
    {
        return $this->app->request()->getBody();
    }

    public function reply($data)
    {
        echo $data;
    }
}
