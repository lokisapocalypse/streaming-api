<?php

namespace tests\integration;

use Fusani\Streaming\SlimTestCase;
use Fusani\Streaming\SlimTransport;

class UserServiceServerTest extends SlimTestCase
{
    public function testServer()
    {
        $transport = new SlimTransport($this->app);
        $this->container['UserServiceServer']->setTransport($transport);

        $request = [
            'jsonrpc' => '2.0',
            'method' => 'testingMethod',
            'params' => [],
            'id' => rand(),
        ];
        $this->post('/rpc/UserService', json_encode($request));

        $response = json_decode($this->response->body(), true);
        $this->assertEquals('Method not found', $response['error']['message']);
    }
}
