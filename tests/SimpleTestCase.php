<?php

namespace Fusani\Streaming;

use PHPUnit_Framework_TestCase;

class mockPdo extends \PDO
{
    public function __construct()
    {
    }
}

class SimpleTestCase extends PHPUnit_Framework_TestCase
{
    protected $container;

    public function setup()
    {
        $this->container = new Infrastructure\Container();

        // Set some keys
        $this->container['EntityManager'] = function ($container) {
            return $this->getMockBuilder('Doctrine\Common\Persistence\ObjectManager')
                ->getMock();
        };

        $this->container['Pdo'] = function ($container) {
            return new mockPdo();
        };

        $this->container['config'] = [
            'sitemap' => ['path' => '/tmp/test-sitemap.xml', 'url' => 'http://www.no.com'],
            'guidebox' => ['apikey' => 3],
        ];
    }
}
