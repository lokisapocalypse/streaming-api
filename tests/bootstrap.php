<?php

// Unit tests should display extra error information.
error_reporting(E_ALL ^ E_NOTICE);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
date_default_timezone_set('UTC');

chdir(__DIR__);

// Set up the autoloader.
if (!$loader = @require '../vendor/autoload.php') {
    die('You must set up the project dependencies, run the following command:' . PHP_EOL .
        'composer.phar install' . PHP_EOL);
}
