<?php

require '../vendor/autoload.php';

use Fusani\Streaming\Infrastructure;
use Slim\Slim;

// Prepare Pimple.
$container = new Infrastructure\Container();
$container['config'] = require '../config/local.php';

// Prepare app.
$app = new Slim($container['config']['slim']);
$container['app'] = $app;

require '../src/routes.php';

// Run the Slim application.
$app->run();
