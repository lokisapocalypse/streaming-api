<?php

namespace Fusani\Streaming\Domain\Model\User;

use Doctrine\Common\Collections\ArrayCollection;

class User
{
    protected $id;
    protected $email;
    protected $firstname;
    protected $identity;
    protected $lastname;

    public function __construct($identity, $firstname, $lastname, $email)
    {
        $this->email = $email;
        $this->firstname = $firstname;
        $this->identity = $identity;
        $this->lastname = $lastname;
    }

    public function getUserInterest()
    {
        return [
            'email' => $this->email,
            'firstname' => $this->firstname,
            'identity' => $this->identity,
            'lastname' => $this->lastname,
        ];
    }
}
