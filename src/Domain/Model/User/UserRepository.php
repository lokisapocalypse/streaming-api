<?php

namespace Fusani\Streaming\Domain\Model\User;

interface UserRepository
{
    public function add(User $user);
    public function flush();
    public function oneOfIdentity($identity);
}
