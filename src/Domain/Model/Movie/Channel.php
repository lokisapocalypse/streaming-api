<?php

namespace Fusani\Streaming\Domain\Model\Movie;

class Channel
{
    protected $id;
    protected $externalIds;
    protected $liveStreams;
    protected $movie;
    protected $posters;
    protected $primary;
    protected $socialMedia;
    protected $source;
    protected $type;

    public function __construct()
    {
        $this->externalIds = [];
        $this->liveStreams = [];
        $this->posters = [];
        $this->socialMedia = [];
    }
}
