<?php

namespace Fusani\Streaming\Domain\Model\Movie;

class Cast
{
    protected $id;
    protected $actor;
    protected $guest;
    protected $movie;
    protected $name;

    public function __construct($actor, $name)
    {
        $this->actor = $actor;
        $this->name = $name;
    }

    public function identity()
    {
        return $this->actor.$this->name;
    }

    public function ofMovie(Movie $movie)
    {
        $this->movie = $movie;
        return $this;
    }

    public function provideCastInterest()
    {
        return [
            'actor' => $this->actor,
            'name' => $this->name,
        ];
    }
}
