<?php

namespace Fusani\Streaming\Domain\Model\Movie;

class Poster
{
    protected $id;
    protected $channel;
    protected $dimension;
    protected $link;
    protected $movie;
    protected $size;

    public function __construct($link, $size)
    {
        $this->link = $link;
        $this->size = $size;
    }

    public function identity()
    {
        return $this->link.$this->size;
    }

    public function ofMovie(Movie $movie)
    {
        $this->movie = $movie;
        return $this;
    }

    public function providePosterInterest()
    {
        return [
            'link' => $this->link,
            'size' => $this->size,
        ];
    }
}
