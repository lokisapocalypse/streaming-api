<?php

namespace Fusani\Streaming\Domain\Model\Movie;

class Actor
{
    protected $id;
    protected $imdb;
    protected $name;

    public function __construct($name)
    {
        $this->name = $name;
    }
}
