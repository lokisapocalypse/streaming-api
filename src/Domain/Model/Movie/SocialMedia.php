<?php

namespace Fusani\Streaming\Domain\Model\Movie;

class SocialMedia
{
    protected $id;
    protected $channel;
    protected $link;
    protected $movie;
    protected $source;
    protected $sourceId;
}
