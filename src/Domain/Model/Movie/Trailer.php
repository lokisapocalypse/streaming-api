<?php

namespace Fusani\Streaming\Domain\Model\Movie;

class Trailer
{
    protected $id;
    protected $device;
    protected $embeddedLink;
    protected $link;
    protected $movie;
    protected $source;
    protected $type;
}
