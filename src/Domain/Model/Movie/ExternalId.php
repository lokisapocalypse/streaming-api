<?php

namespace Fusani\Streaming\Domain\Model\Movie;

class ExternalId
{
    protected $id;
    protected $channel;
    protected $externalId;
    protected $movie;
    protected $source;

    public function __construct($externalId, $source)
    {
        $this->externalId = $externalId;
        $this->source = $source;
    }

    public function identity()
    {
        return $this->externalId.$this->source;
    }

    public function ofMovie(Movie $movie)
    {
        $this->movie = $movie;
        return $this;
    }

    public function provideExternalIdInterest()
    {
        return [
            'externalId' => $this->externalId,
            'source' => $this->source,
        ];
    }
}
