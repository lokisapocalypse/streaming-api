<?php

namespace Fusani\Streaming\Domain\Model\Movie;

class Title
{
    protected $id;
    protected $movie;
    protected $title;

    public function __construct($title)
    {
        $this->title = $title;
    }

    public function identity()
    {
        return $this->title;
    }

    public function ofMovie(Movie $movie)
    {
        $this->movie = $movie;
        return $this;
    }
}
