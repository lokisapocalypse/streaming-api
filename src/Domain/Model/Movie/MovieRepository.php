<?php

namespace Fusani\Streaming\Domain\Model\Movie;

interface MovieRepository
{
    public function add(Movie $movie);
    public function flush();
    public function latestGuideboxIndex();
}
