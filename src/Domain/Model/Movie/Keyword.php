<?php

namespace Fusani\Streaming\Domain\Model\Movie;

class Keyword
{
    protected $id;
    protected $keyword;
    protected $movie;

    public function __construct($keyword)
    {
        $this->keyword = $keyword;
    }

    public function identity()
    {
        return $this->keyword;
    }

    public function ofMovie(Movie $movie)
    {
        $this->movie = $movie;
        return $this;
    }
}
