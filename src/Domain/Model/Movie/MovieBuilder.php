<?php

namespace Fusani\Streaming\Domain\Model\Movie;

use Fusani\Movies\Domain\Model\Movie as FusaniMovie;

class MovieBuilder
{
    public function buildFromMovieObject(FusaniMovie\Movie $movie)
    {
        $interest = $movie->provideMovieWithSourcesConsolidatedInterest();

        $movie = new Movie(
            $interest['title'],
            $interest['type'],
            $interest['year']
        );
        $movie->addExternalId($interest['id'], 'Guidebox');

        foreach ($interest['alternateTitles'] as $title) {
            $movie->addAlternateTitle($title);
        }

        $movie->setRating($interest['rating']);

        foreach ($interest['cast'] as $cast) {
            $movie->addCast($cast);
        }

        foreach ($interest['directors'] as $director) {
            $movie->addDirector($director);
        }

        foreach ($interest['keywords'] as $keyword) {
            $movie->addKeyword($keyword);
        }

        $movie->setPlot($interest['plot']);

        foreach ($interest['posters'] as $poster) {
            $movie->addPoster($poster['link'], $poster['type']);
        }

        foreach ($interest['reviews'] as $review) {
            $movie->addReview($review);
        }

        foreach ($interest['sources'] as $source) {
            $movie->addSource($source);
        }

        return $movie;
    }

    public function updateWithMovieObject(Movie $original, Movie $new)
    {
        $originalInterest = $original->provideGuideboxMovieInterest();
        $newInterest = $new->provideGuideboxMovieInterest();

        foreach ($newInterest['alternateTitles'] as $alternateTitle) {
            $original->addAlternateTitle($alternateTitle);
        }

        foreach ($newInterest['cast'] as $cast) {
            $original->addCast($cast);
        }

        foreach ($newInterest['directors'] as $director) {
            $original->addDirector($director);
        }

        foreach ($newInterest['externalIds'] as $externalId) {
            $original->addExternalId($externalId);
        }

        foreach ($newInterest['keywords'] as $keyword) {
            $original->addKeyword($keyword);
        }

        foreach ($newInterest['posters'] as $poster) {
            $original->addPoster($poster);
        }

        foreach ($newInterest['sources'] as $source) {
            $original->addSource($source);
        }

        $original->setPlot($newInterest['plot'])
            ->setRating($newInterest['rating'])
            ->setTitle($newInterest['title'])
            ->setType($newInterest['type'])
            ->setYear($newInterest['year']);

        return $original;
    }
}
