<?php

namespace Fusani\Streaming\Domain\Model\Movie;

class Director
{
    protected $id;
    protected $imdb;
    protected $movie;
    protected $name;

    public function __construct($name)
    {
        $this->name = $name;
    }

    public function director()
    {
        return $this->name;
    }

    public function identity()
    {
        return $this->name.$this->imdb;
    }

    public function ofMovie(Movie $movie)
    {
        $this->movie = $movie;
        return $this;
    }
}
