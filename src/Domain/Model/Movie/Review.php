<?php

namespace Fusani\Streaming\Domain\Model\Movie;

class Review
{
    protected $id;
    protected $author;
    protected $externalId;
    protected $link;
    protected $movie;
    protected $review;
    protected $source;
    protected $title;

    public function __construct($author, $review, $title, $link)
    {
        $this->author = $author;
        $this->link = $link;
        $this->review = $review;
        $this->title = $title;
    }

    public function identity()
    {
        return $this->author.$this->link.$this->review.$this->title;
    }

    public function ofMovie(Movie $movie)
    {
        $this->movie = $movie;
        return $this;
    }
}
