<?php

namespace Fusani\Streaming\Domain\Model\Movie;

class Format
{
    protected $id;
    protected $format;
    protected $preOrder;
    protected $price;
    protected $source;
    protected $type;
}
