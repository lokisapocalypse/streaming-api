<?php

namespace Fusani\Streaming\Domain\Model\Movie;

class Movie
{
    protected $id;
    protected $alternateTitles;
    protected $cast;
    protected $channels;
    protected $containerShow;
    protected $dayAired;
    protected $directors;
    protected $episodes;
    protected $externalIds;
    protected $duration;
    protected $episode;
    protected $genre;
    protected $inTheaters;
    protected $movie;
    protected $movieType;
    protected $network;
    protected $path;
    protected $preOrder;
    protected $plot;
    protected $posters;
    protected $productionCode;
    protected $rating;
    protected $releaseDate;
    protected $releaseYear;
    protected $reviews;
    protected $season;
    protected $shadow;
    protected $socialMedia;
    protected $sources;
    protected $special;
    protected $statistics;
    protected $status;
    protected $tags;
    protected $title;
    protected $timeAired;
    protected $trailers;
    protected $type;
    protected $writers;

    public function __construct($title, $type, $year)
    {
        $this->alternateTitles = [];
        $this->cast = [];
        $this->channels = [];
        $this->directors = [];
        $this->episodes = [];
        $this->externalIds = [];
        $this->genre = [];
        $this->keywords = [];
        $this->posters = [];
        $this->reviews = [];
        $this->socialMedia = [];
        $this->sources = [];
        $this->statistics = [];
        $this->tags = [];
        $this->trailers = [];
        $this->writers = [];

        $this->path = sprintf(
            '/%s/%s/%d',
            $type == 'movie' ? 'm' : 's',
            strtolower(preg_replace('/\-(\-)+/', '-', preg_replace('/[^A-Za-z0-9]/', '-', $title))),
            $year
        );
        $this->releaseYear = $year;
        $this->title = $title;
        $this->type = $type;
    }

    public function addAlternateTitle($title)
    {
        $alternateTitle = new Title($title);
        $alternateTitle->ofMovie($this);

        foreach ($this->alternateTitles as $at) {
            if ($at->identity() == $alternateTitle->identity()) {
                return $this;
            }
        }

        $this->alternateTitles[] = $alternateTitle;

        return $this;
    }

    public function addCast(array $cast)
    {
        $cast = new Cast($cast['actor'], $cast['character']);
        $cast->ofMovie($this);

        foreach ($this->cast as $c) {
            if ($c->identity() == $cast->identity()) {
                return $this;
            }
        }

        $this->cast[] = $cast;
        return $this;
    }

    public function addDirector($name)
    {
        $director = new Director($name);
        $director->ofMovie($this);

        foreach ($this->directors as $d) {
            if ($d->identity() == $director->identity()) {
                return $this;
            }
        }

        $this->directors[] = $director;
        return $this;
    }

    public function addExternalId($id, $source)
    {
        $externalId = new ExternalId($id, $source);
        $externalId->ofMovie($this);

        foreach ($this->externalIds as $extId) {
            if ($extId->identity() == $externalId->identity()) {
                return $this;
            }
        }

        $this->externalIds[] = $externalId;
        return $this;
    }

    public function addKeyword($word)
    {
        $keyword = new Keyword($word);
        $keyword->ofMovie($this);

        foreach ($this->keywords as $k) {
            if ($k->identity() == $keyword->identity()) {
                return $this;
            }
        }

        $this->keywords[] = $keyword;
        return $this;
    }

    public function addPoster($link, $size)
    {
        $poster = new Poster($link, $size);
        $poster->ofMovie($this);

        foreach ($this->posters as $p) {
            if ($p->identity() == $poster->identity()) {
                return $this;
            }
        }

        $this->posters[] = $poster;
        return $this;
    }

    public function addReview(array $review)
    {
        $review = new Review(
            $review['author'],
            $review['review'],
            $review['title'],
            $review['link']
        );
        $review->ofMovie($this);

        foreach ($this->reviews as $r) {
            if ($r->identity() == $review->identity()) {
                return $this;
            }
        }

        $this->reviews[] = $review;
        return $this;
    }

    public function addSource(array $sourceDetails)
    {
        $source = new Source(
            $sourceDetails['type'],
            $sourceDetails['name'],
            $sourceDetails['link'],
            $sourceDetails['details']
        );
        $source->ofMovie($this);

        foreach ($this->sources as $s) {
            if ($s->identity() == $source->identity()) {
                return $this;
            }
        }

        $this->sources[] = $source;
        return $this;
    }

    public function identity()
    {
        return $this->id;
    }

    public function provideGuideboxMovieInterest()
    {
        $alternateTitles = array_map(function ($a) {
            return $a->identity();
        }, $this->alternateTitles);

        $cast = array_map(function ($c) {
            return $c->provideCastInterest();
        }, $this->cast);

        $directors = array_map(function ($d) {
            return $d->director();
        }, $this->directors);

        $externalIds = [];

        foreach ($this->externalIds as $externalId) {
            $interest = $externalId->provideExternalIdInterest();

            if ($interest['source'] != 'Guidebox Index') {
                $externalIds[] = $interest;
            }
        }

        $keywords = array_map(function ($k) {
            return $k->identity();
        }, $this->keywords);

        $posters = array_map(function ($p) {
            return $p->providePosterInterest();
        }, $this->posters);

        $sources = array_map(function ($s) {
            return $s->provideSourceInterest();
        }, $this->sources);

        return [
            'alternateTitles' => $alternateTitles,
            'cast' => $cast,
            'directors' => $directors,
            'externalIds' => $externalIds,
            'keywords' => $keywords,
            'plot' => $this->plot,
            'posters' => $posters,
            'rating' => $this->rating,
            'sources' => $sources,
            'title' => $this->title,
            'type' => $this->type,
            'year' => $this->releaseYear,
        ];
    }

    public function provideSitemapInterest()
    {
        return [
            'title' => $this->title,
            'type' => $this->type,
            'year' => $this->releaseYear,
        ];
    }

    public function setPlot($plot)
    {
        $this->plot = $plot;
        return $this;
    }

    public function setRating($rating)
    {
        $this->rating = $rating;
        return $this;
    }

    public function setTitle($title)
    {
        $this->title = $title;
        return $this;
    }

    public function setType($type)
    {
        $this->type = $type;
        return $this;
    }

    public function setYear($year)
    {
        $this->releaseYear = $year;
        return $this;
    }
}
