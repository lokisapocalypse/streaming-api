<?php

namespace Fusani\Streaming\Domain\Model\Movie;

class Statistic
{
    protected $id;
    protected $device;
    protected $movie;
    protected $numAvailable;
    protected $type;
}
