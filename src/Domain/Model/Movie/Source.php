<?php

namespace Fusani\Streaming\Domain\Model\Movie;

class Source
{
    protected $id;
    protected $details;
    protected $device;
    protected $formats;
    protected $link;
    protected $movie;
    protected $source;
    protected $type;

    public function __construct($type, $source, $link, array $details = [])
    {
        $this->details = $details;
        $this->formats = [];
        $this->link = $link;
        $this->source = $source;
        $this->type = $type;
    }

    public function identity()
    {
        return $this->link.$this->source.$this->type;
    }

    public function ofMovie(Movie $movie)
    {
        $this->movie = $movie;
        return $this;
    }

    public function provideSourceInterest()
    {
        return [
            'details' => $this->details,
            'link' => $this->link,
            'source' => $this->source,
            'type' => $this->type,
        ];
    }
}
