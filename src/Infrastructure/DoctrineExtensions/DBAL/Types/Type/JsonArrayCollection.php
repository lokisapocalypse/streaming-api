<?php

namespace Fusani\Streaming\Infrastructure\DoctrineExtensions\DBAL\Types\Type;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\DBAL\Types\JsonArrayType;
use Doctrine\DBAL\Platforms\AbstractPlatform;

// This adds a new serialization type to our doctine models
// It's just like json_array, except wrapped in an ArrayCollection
class JsonArrayCollection extends JsonArrayType
{
    const JSON_ARRAY_COLLECTION = 'json_array_collection';

    public function convertToDatabaseValue($value, AbstractPlatform $platform)
    {
        if (null === $value) {
            return null;
        }

        return parent::convertToDatabaseValue($value->getValues(), $platform);
    }

    public function convertToPHPValue($value, AbstractPlatform $platform)
    {
        $value = parent::convertToPHPValue($value, $platform);

        return new ArrayCollection($value);
    }

    public function getName()
    {
        return JsonArrayCollection::JSON_ARRAY_COLLECTION;
    }
}
