<?php

namespace Fusani\Streaming\Infrastructure;

use Fusani\Streaming\Application;
use Pimple;

/**
 * @codeCoverageIgnore
 */
class ServiceContainer extends Pimple
{
    public function __construct(Container $container)
    {
        parent::__construct();

        $this['app'] = function () use ($container) {
            return $container['app'];
        };

        $this['MovieService'] = function () use ($container) {
            $factory = new Application\MovieServiceFactory();
            return $factory->createService($container);
        };

        $this['UserService'] = function () use ($container) {
            $factory = new Application\UserServiceFactory();
            return $factory->createService($container);
        };
    }
}
