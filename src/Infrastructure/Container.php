<?php
namespace Fusani\Streaming\Infrastructure;

use Doctrine\Common;
use Doctrine\DBAL;
use Doctrine\ORM;
use Doctrine\Search;
use Fusani\Streaming\Ui\Server;
use Pimple;

class Container extends Pimple
{
    public function __construct()
    {
        parent::__construct();

        $this->persistence();
        $this->servers();
    }

    protected function persistence()
    {
        $this['EntityManager'] = function ($container) {
            $dbParams = $container['config']['doctrine'];

            // More configuration!
            $paths = array('../src/Domain/Model');
            $proxyDir = '../data/DoctrineORMModule/Proxy';
            $isDevMode = $container['config']['slim']['debug'];

            // Event manager
            $evm = new Common\EventManager();

            $config = ORM\Tools\Setup::createAnnotationMetadataConfiguration(
                $paths,
                $isDevMode,
                $proxyDir
            );
            $driver = $config->getMetadataDriverImpl();

            $namespaces = array(
                'src/Infrastructure/Mapping/Doctrine/' => 'Fusani\Streaming\Domain\Model',
            );
            $yamlDriver = new ORM\Mapping\Driver\SimplifiedYamlDriver($namespaces);
            $config->setMetadataDriverImpl($yamlDriver);

            return ORM\EntityManager::create($dbParams, $config, $evm);
        };

        $this['Pdo'] = function ($container) {
            $config = $container['config']['pdo'];
            // Collapse the DSN properties
            array_walk(
                $config['dsn'],
                function (&$value, $key) {
                    $value = "$key=$value";
                }
            );
            $db = new \PDO(
                implode(';', $config['dsn']),
                $config['username'],
                $config['password'],
                $config['options']
            );
            return $db;
        };
    }

    /**
     * This function creates all the servers that this api will use.
     *
     * @return void
     */
    protected function servers()
    {
        // create the service container to get different services
        $serviceContainer = new ServiceContainer($this);

        // and the servers that use the service container
        $this['UserServiceServer'] = function () use ($serviceContainer) {
            return new Server\UserServiceServer($serviceContainer);
        };
    }
}
