<?php
namespace Fusani\Streaming\Infrastructure\JsonRpc\Transport;

use JsonRpc;

class BasicServer extends JsonRpc\Transport\BasicServer
{
    /**
     * Override this method to not include an "exit".
     * Let Slim handle that.
     */
    public function reply($data)
    {
        echo $data;
    }
}
