<?php

namespace Fusani\Streaming\Infrastructure\Persistence\Doctrine;

use Doctrine\Common\Persistence\ObjectManager;
use Fusani\Streaming\Domain\Model\User;
use Fusani\Streaming\Infrastructure;

class UserRepository implements User\UserRepository
{
    protected $objectManager;
    protected $objectRepository;

    public function __construct(ObjectManager $objectManager)
    {
        $this->objectManager = $objectManager;
        $this->objectRepository = $objectManager->getRepository(
            'Fusani\Streaming\Domain\Model\User\User'
        );
    }

    /**
     * This function adds a new user to the database.
     *
     * @param User\User $user : the new user object to add
     * @return void
     */
    public function add(User\User $user)
    {
        $this->objectManager->persist($user);
    }

    /**
     * This function saves a new user to the database.
     *
     * @return void
     */
    public function flush()
    {
        $this->objectManager->flush();
    }

    public function oneOfIdentity($identity)
    {
        return $this->objectRepository->findOneBy(array('identity' => $identity));
    }
}
