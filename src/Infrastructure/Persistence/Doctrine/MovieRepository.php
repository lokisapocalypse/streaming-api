<?php

namespace Fusani\Streaming\Infrastructure\Persistence\Doctrine;

use Doctrine\Common\Persistence\ObjectManager;
use Fusani\Streaming\Domain\Model\Movie;
use Fusani\Streaming\Infrastructure;
use Pdo;

class MovieRepository implements Movie\MovieRepository
{
    protected $objectManager;
    protected $objectRepository;
    protected $pdo;

    public function __construct(ObjectManager $objectManager, Pdo $pdo)
    {
        $this->objectManager = $objectManager;
        $this->objectRepository = $objectManager->getRepository(
            'Fusani\Streaming\Domain\Model\Movie\Movie'
        );
        $this->pdo = $pdo;
    }

    /**
     * This function adds a new movie to the database.
     *
     * @param Movie\Movie $movie : the new movie object to add
     * @return void
     */
    public function add(Movie\Movie $movie)
    {
        $this->objectManager->persist($movie);
    }

    /**
     * This function saves a new movie to the database.
     *
     * @return void
     */
    public function flush()
    {
        $this->objectManager->flush();
    }

    public function lastGuideboxUpdateTime()
    {
        $query = 'SELECT value FROM api_metadata WHERE `key` = "Guidebox Index Time"';
        $sth = $this->pdo->prepare($query);
        $sth->execute();
        $result = $sth->fetch();

        if (empty($result['value'])) {
            return 0;
        }

        return $result['value'];
    }

    public function latestGuideboxIndex()
    {
        $query = 'SELECT MAX(CAST(external_id AS UNSIGNED)) AS latestIndex
            FROM external_id
            WHERE source = "Guidebox Index"';

        $sth = $this->pdo->prepare($query);
        $sth->execute();

        $result = $sth->fetch();

        if (empty($result['latestIndex'])) {
            return -1;
        }

        return $result['latestIndex'];
    }

    public function latestToBeUpdated($limit = 100)
    {
        $query = 'SELECT id
            FROM movie
            ORDER BY last_updated
            LIMIT :limit';

        $sth = $this->pdo->prepare($query, [':limit' => $limit]);
        $sth->execute();

        $indices = array_map(function ($data) {
            return $data['id'];
        }, $sth->fetchAll());

        return $indices;
    }

    public function oneOfGuideboxId($id)
    {
        $query = $this->objectRepository->createQueryBuilder()
            ->select('m')
            ->from(Movie\Movie::class, 'm')
            ->join('m.externalIds', 'e')
            ->where('e.externalId = :externalId AND e.source = :source')
            ->setParameter(':externalId', $id)
            ->setParameter(':source', 'Guidebox');

        return $query->getQuery->getResult();
    }

    public function oneOfId($id)
    {
        return $this->objectRepository->findOneBy(['id' => $id]);
    }
}
