<?php

namespace Fusani\Streaming\Infrastructure\Persistence\InMemory;

use Fusani\Streaming\Domain\Model\Movie;

class MovieRepository implements Movie\MovieRepository
{
    protected $movies;

    public function __construct()
    {
        $this->movies = [];
    }

    public function add(Movie\Movie $movie)
    {
        $this->movies[] = $movie;
    }

    public function count()
    {
        return count($this->movies);
    }

    /**
     * @codeCoverageIgnore
     */
    public function flush()
    {
        // the flush, it does nothing
    }

    public function latestGuideboxIndex()
    {
        return $this->count() - 1;
    }
}
