<?php

namespace Fusani\Streaming\Infrastructure\Persistence\InMemory;

use Fusani\Streaming\Domain\Model\User;

class UserRepository implements User\UserRepository
{
    protected $users;

    public function __construct()
    {
        $this->users = [];
    }

    public function add(User\User $user)
    {
        $interest = $user->getUserInterest();
        $this->users[$interest['identity']] = $user;
    }

    public function count()
    {
        return count($this->users);
    }

    public function flush()
    {
        // the flush, it does nothing
    }

    public function oneOfIdentity($identity)
    {
        return $this->users[$identity];
    }
}
