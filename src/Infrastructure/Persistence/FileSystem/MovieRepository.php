<?php

namespace Fusani\Streaming\Infrastructure\Persistence\FileSystem;

use Fusani\Movies\Domain\Model\Movie as FusaniMovie;
use Fusani\Streaming\Domain\Model\Movie;

/**
 * @codeCoverageIgnore
 */
class MovieRepository implements Movie\MovieRepository
{
    protected $content;
    protected $data;
    protected $path;
    protected $url;

    public function __construct($path, $url)
    {
        if (file_exists($path)) {
            $this->content = file($path);
            unset($this->content[sizeof($this->content) - 1]);
        } else {
            $this->content = [];
        }

        $this->data = [];
        $this->path = $path;
        $this->url = $url;
    }

    public function add(Movie\Movie $movie)
    {
        $interest = $movie->provideSitemapInterest();

        $this->data[] = sprintf(
            '/%s/%s/%s',
            $interest['type'] == 'movie' ? 'm' : 's',
            strtolower(preg_replace('/\-(\-)+/', '-', preg_replace('/[^A-Za-z0-9]/', '-', $interest['title']))),
            $interest['year']
        );
    }

    public function flush()
    {
        $content = '';

        if (empty($this->content)) {
            $content .= "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n";
            $content .= "<urlset xmlns=\"http://www.sitemaps.org/schemas/sitemap/0.9\">\n";
        }

        foreach ($this->data as $data) {
            $content .= "    <url>\n";
            $content .= "        <loc>{$this->url}$data</loc>\n";
            $content .= "    </url>\n";
        }

        $content .= "</urlset>\n";

        file_put_contents($this->path, implode('', $this->content).$content);
    }

    public function latestGuideboxIndex()
    {
        throw new FusaniMovie\NotYetImplementedException();
    }
}
