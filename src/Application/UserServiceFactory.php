<?php
namespace Fusani\Streaming\Application;

use Fusani\Streaming\Infrastructure;
use Fusani\Streaming\Infrastructure\Persistence\Doctrine;

class UserServiceFactory
{
    /**
     * This function creates a user service object. The user service object
     * just works with doctrine right now.
     *
     * @param Infrastructure\Container $container : a container object, used to get doctrine
     * @return new UserService object
     */
    public function createService(Infrastructure\Container $container)
    {
        $mailService = new MailService();
        $repository = new Doctrine\UserRepository(
            $container['EntityManager']
        );

        return new UserService($repository, $mailService);
    }
}
