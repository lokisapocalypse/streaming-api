<?php

namespace Fusani\Streaming\Application;

class MailService
{
    /**
     * @codeCoverageIgnore
     */
    protected function mail($to, $from, $subject, $message)
    {
        $headers = "From: $from\r\n";
        $headers .= "Reply-To: $from\r\n";
        return mail($to, $subject, $message, $headers);
    }
}
