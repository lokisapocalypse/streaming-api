<?php

namespace Fusani\Streaming\Application;

use Fusani\Streaming\Domain\Model\User;

class UserService
{
    protected $userRepository;

    public function __construct(User\UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function createUser($identity, $firstname, $lastname, $email)
    {
        $user = $this->userRepository->oneOfIdentity($identity);

        if (!$user) {
            $user = new User\User($identity, $firstname, $lastname, $email);
            $this->userRepository->add($user);
            $this->userRepository->flush();
        }

        return $user->getUserInterest();
    }
}
