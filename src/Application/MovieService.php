<?php

namespace Fusani\Streaming\Application;

use Fusani\Movies\Domain\Model\Movie;
use Fusani\Streaming\Domain\Model\Movie as StreamingMovie;
use Fusani\Streaming\Infrastructure\Persistence\FileSystem;

class MovieService
{
    const GUIDEBOX_MAX = 250;
    const UPDATE_MAX = 100;

    protected $guideboxRepository;
    protected $movieBuilder;
    protected $movieRepository;

    public function __construct(
        StreamingMovie\MovieRepository $movieRepository,
        Movie\MovieRepository $guideboxRepository,
        StreamingMovie\MovieBuilder $movieBuilder
    ) {
        $this->guideboxRepository = $guideboxRepository;
        $this->movieBuilder = $movieBuilder;
        $this->movieRepository = $movieRepository;
    }

    public function importMovies()
    {
        $index = $this->movieRepository->latestGuideboxIndex();

        $movies = $this->guideboxRepository->many(
            $index + 1,
            self::GUIDEBOX_MAX,
            'movie'
        );

        $counter = $index + 1;

        foreach ($movies as $m) {
            sleep(1);
            $movie = $this->guideboxRepository->oneOfId($m->identity());
            $movie = $this->movieBuilder->buildFromMovieObject($movie);
            $movie->addExternalId($counter, 'Guidebox Index');

            $this->movieRepository->add($movie);

            $counter++;
        }

        $this->movieRepository->flush();
    }

    public function updateMovies()
    {
        $indices = $this->movieRepository->latestToBeUpdated(self::UPDATE_MAX);

        foreach ($indices as $index) {
            sleep(1);
            $movie = $this->movieRepository->oneOfId($index);
            $movie = $this->movieBuilder->updateWithMovieObject(
                $movie,
                $this->guideboxRepository->oneOfId($movie->guideboxIdentity())
            );
            $movie = $this->movieBuilder->updateWithMovieObject(
                $movie,
                $this->theMovieDbRepository->oneOfId($movie->theMovieDbIdentity())
            );
        }

        $this->movieRepository->flush();
    }

    public function updateMetadataFromGuidebox()
    {
        // what's the last time we made changes here?
        $latestTime = $this->movieRepository->lastGuideboxUpdateTime();
        $currentTime = $this->guideboxRepository->currentTime();

        $movieIds = $movies = $showIds = [];

        // there are six different queries we have to make
        $movieIds = $this->guideboxRepository->manyMoviesWithChanges($latestTime);
        sleep(1);

        $movieIds = array_merge(
            $movieIds,
            $this->guideboxRepository->withUpdatedMovies()
                ->manyMoviesWithChanges($latestTime)
        );
        sleep(1);

        $showIds = $this->guideboxRepository->searchForShows()
            ->withNewEpisodes()
            ->manyMoviesWithChanges($latestTime);
        sleep(1);

        $showIds = array_merge(
            $showIds,
            $this->guideboxRepository->withNewMovies()
                ->manyMoviesWithChanges($latestTime)
        );
        sleep(1);

        $showIds = array_merge(
            $showIds,
            $this->guideboxRepository->withUpdatedEpisodes()
                ->manyMoviesWithChanges($latestTime)
        );
        sleep(1);

        $showIds = array_merge(
            $showIds,
            $this->guideboxRepository->withUpdatedMovies()
                ->manyMoviesWithChanges($latestTime)
        );
        sleep(1);

        $this->guideboxRepository->searchForMovies();
        foreach ($movieIds as $id) {
            $movies[] = $this->guideboxRepository->oneOfId($id['id']);
            sleep(1);
        }

        $this->guideboxRepository->searchForShows();
        foreach ($showIds as $id) {
            $movies[] = $this->guideboxRepository->oneOfId($id['id']);
            sleep(1);
        }

        // now we need to get our version of the data and update them
        foreach ($movies as $fusaniMovie) {
            $hasNewStreamingOptions = false;
            $movie = $this->movieRepository->oneOfGuideboxId($fusaniMovie->identity());
            $fusaniMovie = $this->movieBuilder->buildFromMovieObject($fusaniMovie);

            if (!empty($movie)) {
                if ($movie->provideGuideboxMovieInterest() != $fusaniMovie->provideGuideboxMovieInterest()) {
                    $movie = $this->movieBuilder->updateWithMovieObject($movie, $fusaniMovie);
                    $hasNewStreamingOptions = $movie->hasNewStreamingOptions();
                }
            } else {
                $interest = $fusaniMovie->provideMovieInterest();
                $movie = $this->movieBuilder->buildFromMovieObject($fusaniMovie);

                foreach ($interest['externalIds'] as $externalId) {
                    if ($externalId['source'] == 'The Movie DB') {
                        $movie = $this->movieBuilder->updateWithMovieObject(
                            $movie,
                            $this->
                        );
                    }
                }

                $hasNewStreamingOptions = true;
                $this->movieRepository->add($movie);
            }
        }

        $this->movieRepository->updateGuideboxUpdateTime($currentTime);
        $this->movieRepository->flush();
    }
}
