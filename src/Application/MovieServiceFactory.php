<?php
namespace Fusani\Streaming\Application;

use Fusani\Movies\Infrastructure\Persistence\Guidebox;
use Fusani\Streaming\Domain\Model\Movie;
use Fusani\Streaming\Infrastructure;
use Fusani\Streaming\Infrastructure\Persistence\Doctrine;
use Fusani\Streaming\Infrastructure\Persistence\FileSystem;

class MovieServiceFactory
{
    /**
     * This function creates a movie service object. The movie service object
     * just works with doctrine right now.
     *
     * @param Infrastructure\Container $container : a container object, used to get doctrine
     * @return new MovieService object
     */
    public function createService(Infrastructure\Container $container)
    {
        $config = $container['config'];

        $repository = new Doctrine\MovieRepository(
            $container['EntityManager'],
            $container['Pdo']
        );

        $guideboxRepositoryFactory = new Guidebox\MovieRepositoryFactory();

        return new MovieService(
            $repository,
            $guideboxRepositoryFactory->createRepository($config['guidebox']['apikey']),
            new Movie\MovieBuilder()
        );
    }
}
