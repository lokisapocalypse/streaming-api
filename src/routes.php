<?php

namespace Fusani\Streaming;

$app->group('/rpc', function () use ($app, $container) {
    $app->post('/UserService', function () use ($container) {
        $container['UserServiceServer']->receive();
    });
});
