<?php
namespace Fusani\Streaming\Ui\Server;

use JsonRpc;
use Fusani\Streaming\Infrastructure;

class UserServiceServer extends JsonRpc\Server
{
    public function __construct(Infrastructure\ServiceContainer $container)
    {
        $container['app']->response->headers
            ->set('Content-Type', 'application/json');

        $sampleService = $container['UserService'];

        $transport = new Infrastructure\JsonRpc\Transport\BasicServer();

        parent::__construct($sampleService, $transport);
    }
}
