<?php

namespace DoctrineMigration;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20160829172901 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->addSql(
            'ALTER TABLE movie ADD COLUMN movie_id INT UNSIGNED DEFAULT NULL,
                ADD CONSTRAINT fk_movie_movie_id FOREIGN KEY (movie_id) REFERENCES movie (id) ON UPDATE CASCADE ON DELETE RESTRICT,
                ADD COLUMN season TINYINT UNSIGNED DEFAULT NULL,
                ADD COLUMN episode TINYINT UNSIGNED DEFAULT NULL,
                ADD COLUMN production_code VARCHAR(50) DEFAULT NULL,
                ADD COLUMN is_shadow BOOLEAN DEFAULT 0,
                ADD COLUMN is_special BOOLEAN DEFAULT 0'
        );
        $this->addSql('ALTER TABLE cast ADD COLUMN is_guest BOOLEAN DEFAULT 0');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $this->addSql(
            'ALTER TABLE movie DROP FOREIGN KEY fk_movie_movie_id,
                DROP COLUMN movie_id,
                DROP COLUMN season,
                DROP COLUMN episode,
                DROP COLUMN production_code,
                DROP COLUMN is_shadow,
                DROP COLUMN is_special'
        );
        $this->addSql('ALTER TABLE cast DROP COLUMN is_guest');
    }
}
