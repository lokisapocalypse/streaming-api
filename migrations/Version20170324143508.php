<?php

namespace DoctrineMigration;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170324143508 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql(
            'CREATE TABLE keyword (
                id INT UNSIGNED NOT NULL PRIMARY KEY AUTO_INCREMENT,
                movie_id INT UNSIGNED NOT NULL,
                keyword VARCHAR(255) NOT NULL,
                FOREIGN KEY (movie_id) REFERENCES movie (id) ON UPDATE CASCADE ON DELETE RESTRICT
            )'
        );
        $this->addSql('ALTER TABLE review ADD COLUMN title VARCHAR(600) NOT NULL');
        $this->addSql('ALTER TABLE movie ADD COLUMN path VARCHAR(255) NOT NULL UNIQUE');
        $this->addSql('ALTER TABLE source ADD COLUMN details VARCHAR(1025) DEFAULT NULL');
        $this->addSql('ALTER TABLE cast DROP FOREIGN KEY cast_ibfk_1');
        $this->addSql('ALTER TABLE cast DROP COLUMN actor_id');
        $this->addSql('ALTER TABLE cast ADD COLUMN actor VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE director CHANGE imdb imdb VARCHAR(55) DEFAULT NULL');
        $this->addSql('ALTER TABLE source CHANGE device device VARCHAR(25) DEFAULT NULL');
        $this->addSql('ALTER TABLE source DROP COLUMN identity');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE keyword');
        $this->addSql('ALTER TABLE review DROP COLUMN title');
        $this->addSql('ALTER TABLE movie DROP COLUMN path');
        $this->addSql('ALTER TABLE source DROP COLUMN details');
        $this->addSql('ALTER TABLE cast ADD COLUMN actor_id INT UNSIGNED NOT NULL');
        $this->addSql('ALTER TABLE cast ADD FOREIGN KEY (actor_id) REFERENCES actor (id) ON UPDATE CASCADE ON DELETE RESTRICT');
        $this->addSql('ALTER TABLE cast DROP COLUMN actor');
        $this->addSql('ALTER TABLE director CHANGE imdb imdb VARCHAR(55) NOT NULL');
        $this->addSql('ALTER TABLE source CHANGE device device VARCHAR(25) NOT NULL');
        $this->addSql('ALTER TABLE source ADD COLUMN identity VARCHAR(255) NOT NULL');
    }
}
