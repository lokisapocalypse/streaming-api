<?php

namespace DoctrineMigration;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170330152535 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql(
            'CREATE TABLE api_metadata (
                id INT UNSIGNED NOT NULL PRIMARY KEY AUTO_INCREMENT,
                `key` VARCHAR(50) NOT NULL,
                value VARCHAR(50) NOT NULL,
                UNIQUE (`key`, value)
            )'
        );
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE api_metadata');
    }
}
