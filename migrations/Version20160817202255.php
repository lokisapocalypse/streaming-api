<?php

namespace DoctrineMigration;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20160817202255 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->addSql('ALTER TABLE movie ADD COLUMN `type` VARCHAR(15) NOT NULL');
        $this->addSql('ALTER TABLE movie ADD COLUMN status VARCHAR(50) DEFAULT NULL');
        $this->addSql('ALTER TABLE movie ADD COLUMN movie_type VARCHAR(50) DEFAULT NULL');
        $this->addSql('ALTER TABLE movie ADD COLUMN container_show BOOLEAN DEFAULT NULL');
        $this->addSql('ALTER TABLE movie ADD COLUMN network VARCHAR(70) DEFAULT NULL');
        $this->addSql(
            'CREATE TABLE channel (
                id INT UNSIGNED NOT NULL PRIMARY KEY AUTO_INCREMENT,
                movie_id INT UNSIGNED NOT NULL,
                `primary` BOOLEAN DEFAULT NULL,
                source VARCHAR(75) NOT NULL,
                `type` VARCHAR(50) NOT NULL,
                FOREIGN KEY (movie_id) REFERENCES movie (id) ON UPDATE CASCADE ON DELETE RESTRICT
            )'
        );

        $this->addSql(
            'ALTER TABLE external_id
                ADD COLUMN channel_id INT UNSIGNED DEFAULT NULL,
                ADD CONSTRAINT fk_external_id_channel_id FOREIGN KEY (channel_id) REFERENCES channel (id) ON UPDATE CASCADE ON DELETE RESTRICT'
        );

        $this->addSql(
            'CREATE TABLE live_stream (
                id INT UNSIGNED NOT NULL PRIMARY KEY AUTO_INCREMENT,
                channel_id INT UNSIGNED NOT NULL
             )'
        );
        $this->addSql(
            'ALTER TABLE live_stream
                ADD CONSTRAINT fk_live_stream_channel_id FOREIGN KEY (channel_id) REFERENCES channel (id) ON UPDATE CASCADE ON DELETE RESTRICT'
        );

        $this->addSql(
            'ALTER TABLE poster
                ADD COLUMN channel_id INT UNSIGNED DEFAULT NULL,
                ADD CONSTRAINT fk_poster_channel_id FOREIGN KEY (channel_id) REFERENCES channel (id) ON UPDATE CASCADE ON DELETE RESTRICT'
        );

        $this->addSql(
            'ALTER TABLE social_media
                ADD COLUMN channel_id INT UNSIGNED DEFAULT NULL,
                ADD CONSTRAINT fk_social_media_channel_id FOREIGN KEY (channel_id) REFERENCES channel (id) ON UPDATE CASCADE ON DELETE RESTRICT'
        );

        $this->addSql('ALTER TABLE movie ADD COLUMN day_aired VARCHAR(25) DEFAULT NULL');
        $this->addSql('ALTER TABLE movie ADD COLUMN time_aired VARCHAR(25) DEFAULT NULL');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $this->addSql('ALTER TABLE movie DROP COLUMN `type`');
        $this->addSql('ALTER TABLE movie DROP COLUMN status');
        $this->addSql('ALTER TABLE movie DROP COLUMN movie_type');
        $this->addSql('ALTER TABLE movie DROP COLUMN container_show');
        $this->addSql('ALTER TABLE movie DROP COLUMN network');
        $this->addSql('ALTER TABLE social_media DROP FOREIGN KEY fk_social_media_channel_id');
        $this->addSql('ALTER TABLE social_media DROP COLUMN channel_id');
        $this->addSql('ALTER TABLE poster DROP FOREIGN KEY fk_poster_channel_id');
        $this->addSql('ALTER TABLE poster DROP COLUMN channel_id');
        $this->addSql('DROP TABLE live_stream');
        $this->addSql('ALTER TABLE external_id DROP FOREIGN KEY fk_external_id_channel_id');
        $this->addSql('ALTER TABLE external_id DROP COLUMN channel_id');
        $this->addSql('DROP TABLE channel');
        $this->addSql('ALTER TABLE movie DROP COLUMN day_aired');
        $this->addSql('ALTER TABLE movie DROP COLUMN time_aired');
    }
}
