<?php

namespace DoctrineMigration;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20161028134205 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql(
            'ALTER TABLE review ADD COLUMN author VARCHAR(200) DEFAULT NULL,
                ADD COLUMN external_id VARCHAR(55) DEFAULT NULL,
                ADD COLUMN review VARCHAR(60000) DEFAULT NULL'
        );
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql(
            'ALTER TABLE review DROP COLUMN author,
                DROP COLUMN external_id,
                DROP COLUMN review'
        );
    }
}
