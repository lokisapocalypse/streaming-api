<?php

namespace DoctrineMigration;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20160815174954 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->addSql(
            'CREATE TABLE actor (
                id INT UNSIGNED NOT NULL PRIMARY KEY AUTO_INCREMENT,
                imdb VARCHAR(55) NOT NULL,
                name VARCHAR(511) NOT NULL
            )'
        );

        $this->addSql(
            'CREATE TABLE movie (
                id INT UNSIGNED NOT NULL PRIMARY KEY AUTO_INCREMENT,
                duration INT UNSIGNED DEFAULT NULL,
                in_theaters BOOLEAN DEFAULT 0,
                pre_order BOOLEAN DEFAULT 0,
                plot VARCHAR(5000) NOT NULL,
                rating VARCHAR(10) DEFAULT NULL,
                release_date DATE NOT NULL,
                release_year YEAR NOT NULL,
                title VARCHAR(511) NOT NULL
            )'
        );

        $this->addSql(
            'CREATE TABLE cast (
                id INT UNSIGNED NOT NULL PRIMARY KEY AUTO_INCREMENT,
                movie_id INT UNSIGNED NOT NULL,
                actor_id INT UNSIGNED NOT NULL,
                name VARCHAR(511) NOT NULL,
                FOREIGN KEY (actor_id) REFERENCES actor (id) ON UPDATE CASCADE ON DELETE RESTRICT,
                FOREIGN KEY (movie_id) REFERENCES movie (id) ON UPDATE CASCADE ON DELETE RESTRICT
            )'
        );

        $this->addSql(
            'CREATE TABLE director (
                id INT UNSIGNED NOT NULL PRIMARY KEY AUTO_INCREMENT,
                movie_id INT UNSIGNED NOT NULL,
                imdb VARCHAR(55) NOT NULL,
                name VARCHAR(511) NOT NULL,
                FOREIGN KEY (movie_id) REFERENCES movie (id) ON UPDATE CASCADE ON DELETE RESTRICT
            )'
        );

        $this->addSql(
            'CREATE TABLE external_id (
                id INT UNSIGNED NOT NULL PRIMARY KEY AUTO_INCREMENT,
                movie_id INT UNSIGNED NOT NULL,
                external_id VARCHAR(80) NOT NULL,
                source VARCHAR(255) NOT NULL,
                UNIQUE(external_id, source),
                FOREIGN KEY (movie_id) REFERENCES movie (id) ON UPDATE CASCADE ON DELETE RESTRICT
            )'
        );

        $this->addSql(
            'CREATE TABLE genre (
                id INT UNSIGNED NOT NULL PRIMARY KEY AUTO_INCREMENT,
                movie_id INT UNSIGNED NOT NULL,
                `type` VARCHAR(55) NOT NULL,
                FOREIGN KEY (movie_id) REFERENCES movie (id) ON UPDATE CASCADE ON DELETE RESTRICT
            )'
        );

        $this->addSql(
            'CREATE TABLE poster (
                id INT UNSIGNED NOT NULL PRIMARY KEY AUTO_INCREMENT,
                movie_id INT UNSIGNED NOT NULL,
                dimension VARCHAR(15),
                link VARCHAR(511) NOT NULL,
                size VARCHAR(25) NOT NULL,
                FOREIGN KEY (movie_id) REFERENCES movie (id) ON UPDATE CASCADE ON DELETE RESTRICT
            )'
        );

        $this->addSql(
            'CREATE TABLE review (
                id INT UNSIGNED NOT NULL PRIMARY KEY AUTO_INCREMENT,
                link VARCHAR(511) NOT NULL,
                source VARCHAR(255) NOT NULL,
                movie_id INT UNSIGNED NOT NULL,
                FOREIGN KEY (movie_id) REFERENCES movie (id) ON UPDATE CASCADE ON DELETE RESTRICT
            )'
        );

        $this->addSql(
            'CREATE TABLE social_media (
                id INT UNSIGNED NOT NULL PRIMARY KEY AUTO_INCREMENT,
                link VARCHAR(511) NOT NULL,
                source VARCHAR(255) NOT NULL,
                source_id VARCHAR(75) NOT NULL,
                movie_id INT UNSIGNED NOT NULL,
                FOREIGN KEY (movie_id) REFERENCES movie (id) ON UPDATE CASCADE ON DELETE RESTRICT
            )'
        );

        $this->addSql(
            'CREATE TABLE source (
                id INT UNSIGNED NOT NULL PRIMARY KEY AUTO_INCREMENT,
                movie_id INT UNSIGNED NOT NULL,
                device VARCHAR(25) NOT NULL,
                identity VARCHAR(255) NOT NULL,
                link VARCHAR(511) NOT NULL,
                source VARCHAR(255) NOT NULL,
                FOREIGN KEY (movie_id) REFERENCES movie (id) ON UPDATE CASCADE ON DELETE RESTRICT
            )'
        );

        $this->addSql(
            'CREATE TABLE tag (
                id INT UNSIGNED NOT NULL PRIMARY KEY AUTO_INCREMENT,
                name VARCHAR(255) NOT NULL,
                movie_id INT UNSIGNED NOT NULL,
                FOREIGN KEY (movie_id) REFERENCES movie (id) ON UPDATE CASCADE ON DELETE RESTRICT
            )'
        );

        $this->addSql(
            'CREATE TABLE trailer (
                id INT UNSIGNED NOT NULL PRIMARY KEY AUTO_INCREMENT,
                device VARCHAR(25) NOT NULL,
                embedded_link VARCHAR(511) NOT NULL,
                link VARCHAR(511) NOT NULL,
                source VARCHAR(255) NOT NULL,
                `type` VARCHAR(100) NOT NULL,
                movie_id INT UNSIGNED NOT NULL,
                FOREIGN KEY (movie_id) REFERENCES movie (id) ON UPDATE CASCADE ON DELETE RESTRICT
            )'
        );

        $this->addSql(
            'CREATE TABLE title (
                id INT UNSIGNED NOT NULL PRIMARY KEY AUTO_INCREMENT,
                movie_id INT UNSIGNED NOT NULL,
                title VARCHAR(511) NOT NULL,
                INDEX(title),
                FOREIGN KEY (movie_id) REFERENCES movie (id) ON UPDATE CASCADE ON DELETE RESTRICT
            )'
        );

        $this->addSql(
            'CREATE TABLE writer (
                id INT UNSIGNED NOT NULL PRIMARY KEY AUTO_INCREMENT,
                movie_id INT UNSIGNED NOT NULL,
                imdb VARCHAR(55) NOT NULL,
                name VARCHAR(511) NOT NULL,
                FOREIGN KEY (movie_id) REFERENCES movie (id) ON UPDATE CASCADE ON DELETE RESTRICT
            )'
        );

        $this->addSql(
            'CREATE TABLE format (
                id INT UNSIGNED NOT NULL PRIMARY KEY AUTO_INCREMENT,
                source_id INT UNSIGNED NOT NULL,
                format VARCHAR(10) DEFAULT NULL,
                pre_order BOOLEAN DEFAULT 0,
                price FLOAT(5,2) DEFAULT NULL,
                `type` VARCHAR(25) DEFAULT NULL,
                FOREIGN KEY (source_id) REFERENCES source (id) ON UPDATE CASCADE ON DELETE RESTRICT
            )'
        );
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $this->addSql('DROP TABLE format');
        $this->addSql('DROP TABLE writer');
        $this->addSql('DROP TABLE title');
        $this->addSql('DROP TABLE trailer');
        $this->addSql('DROP TABLE tag');
        $this->addSql('DROP TABLE source');
        $this->addSql('DROP TABLE social_media');
        $this->addSql('DROP TABLE review');
        $this->addSql('DROP TABLE poster');
        $this->addSql('DROP TABLE genre');
        $this->addSql('DROP TABLE external_id');
        $this->addSql('DROP TABLE director');
        $this->addSql('DROP TABLE cast');
        $this->addSql('DROP TABLE movie');
        $this->addSql('DROP TABLE actor');
    }
}
