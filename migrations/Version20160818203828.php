<?php

namespace DoctrineMigration;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20160818203828 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->addSql('ALTER TABLE source ADD COLUMN `type` VARCHAR(50) NOT NULL');
        $this->addSql(
            'CREATE TABLE statistic (
                id INT UNSIGNED NOT NULL PRIMARY KEY AUTO_INCREMENT,
                movie_id INT UNSIGNED NOT NULL,
                device VARCHAR(25) NOT NULL,
                num_available SMALLINT UNSIGNED NOT NULL,
                `type` VARCHAR(50) NOT NULL
            )'
        );
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $this->addSql('ALTER TABLE source DROP COLUMN `type`');
        $this->addSql('DROP TABLE statistic');
    }
}
