<?php

use Doctrine\DBAL\Migrations\Tools\Console;
use Fusani\Streaming\Infrastructure;

chdir(__DIR__ . '/..');
require 'vendor/autoload.php';

// Prepare the DI container
$container = new Infrastructure\Container();
$container['config'] = require 'config/local.php';

$em = $container['EntityManager'];

$helperSet = new \Symfony\Component\Console\Helper\HelperSet(array(
    'db' => new \Doctrine\DBAL\Tools\Console\Helper\ConnectionHelper($em->getConnection()),
    'em' => new \Doctrine\ORM\Tools\Console\Helper\EntityManagerHelper($em),
));

$cli = new \Symfony\Component\Console\Application(
    'Doctrine Command Line Interface',
    \Doctrine\ORM\Version::VERSION
);

$cli->setHelperSet($helperSet);

$cli->addCommands([
    new Console\Command\DiffCommand(),
    new Console\Command\ExecuteCommand(),
    new Console\Command\GenerateCommand(),
    new Console\Command\MigrateCommand(),
    new Console\Command\StatusCommand(),
    new Console\Command\VersionCommand()
]);

// Add ORM commands.
\Doctrine\ORM\Tools\Console\ConsoleRunner::addCommands($cli);

$cli->run();
